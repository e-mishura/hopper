object Solution {
  case class Heading(weight: Int, text: String)
  case class Node(heading: Heading, children: List[Node])

  val testInput = List(
    "H1 All About Birds",
    "H2 Kinds of Birds",
    "H3 The Finch",
    "H3 The Swan",
    "H2 Habitats",
    "H3 Wetlands"
  )

  def main(args: Array[String]) {
//    val headings: Iterator[Heading] = scala.io.Source.stdin.getLines.flatMap(parse)
    val headings: Iterator[Heading] = testInput.toIterator.flatMap(parse)
    val outline: Node = toOutline(headings)
    val html: String = toHtml(outline).trim
    println(html)
  }

  /** Converts a list of input headings into nested nodes */
  def toOutline(headings: Iterator[Heading]): Node = {

    // Implement this function. Sample code below builds an
    // outline of only the first heading.

    val topHeaders = getChildren(headings.toStream)
    Node(Heading(0, ""), topHeaders)
  }

  def getChildren(headings: Stream[Heading]): List[Node]  =  headings match {
    case Stream.Empty => Nil
    case hd #:: tail =>
      val (subtree, reminder) = tail.span(_.weight > hd.weight)
//      val subtree = tail.takeWhile(_.weight > hd.weight)
//      val reminder = tail.dropWhile(_.weight > hd.weight)
      val hdChildren = getChildren(subtree)
      val hdNode = Node(hd, hdChildren)
      val restChildren = getChildren(reminder)
      hdNode :: restChildren
  }

  /** Parses a line of input.
    This implementation is correct for all predefined test cases. */
  def parse(record: String): Option[Heading] = {
    val H = "H(\\d+)".r
    record.split(" ", 2) match {
      case Array(H(level), text) =>
        scala.util.Try(Heading(level.toInt, text.trim)).toOption
      case _ => None
    }
  }

  /** Converts a node to HTML.
    This implementation is correct for all predefined test cases. */
  def toHtml(node: Node): String = {
    val childHtml = node.children match {
      case Nil => ""
      case children =>
        "<ol>" + children.map(
          child => "<li>" + toHtml(child) + "</li>"
        ).mkString("\n") + "</ol>"
    }
    val heading =
      if (node.heading.text.isEmpty) ""
      else node.heading.text + "\n"
    heading + childHtml
  }
}
